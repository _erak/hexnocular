/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.3
import QtQuick.Controls 1.3
import QtMultimedia 5.4

import hexnocular.WorkspaceImage 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 1024
    height: 768
    color: "black"

    property int bpm:           120
    property int scale:         10
    property int bpm_fps_25:    1500

    function bpm_to_interval(bpm)
    {
        var bpms = bpm / 60 / 1000
        return Math.round( 1 / bpms );
    }

    Timer {
        property int tick: 0

        interval: bpm_to_interval(bpm_fps_25); running: true; repeat: true
        onTriggered: {
            tick++
            workspaceImage.tick = tick
        }
    }

    WorkspaceImage {
        id: workspaceImage
        width: parent.width
        height: parent.height
    }

    Rectangle {
        id: controls
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 20
        anchors.leftMargin: 20
        width: 190
        height: 200
        color: Qt.rgba(255, 255, 255, 0.8)

        Grid {
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.leftMargin: 10
            width: parent.width
            columns: 2
            spacing: 5

            Label { text: "mode" }
            ComboBox {
                model: [ "calibration", "live" ]
            }

            Label { text: "input" }
            ComboBox {
                model: [ "edges", "kaleidoskop" ]
            }

        }

    }

}
