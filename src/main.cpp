/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <string>
#include <utility>

#include <QObject>
#include <QApplication>
#include <QQuickView>
#include <QQmlApplicationEngine>
#include <QQuickItem>
#include <QDebug>

#include "WorkspaceImage.h"

using namespace std;

int main(int argc, char *argv[])
{    
    // QML initialization: start engine and load qml file
    QApplication app(argc, argv);
    QQmlApplicationEngine qmlEngine;

    // Register QML types
    qmlRegisterType< hexnocular::WorkspaceImage > ("hexnocular.WorkspaceImage",   1, 0, "WorkspaceImage");

    // Scene image / video
    qmlEngine.load( QUrl( QStringLiteral("qml/main.qml")));

    // Execute and wait until exit signal received from UI
    return app.exec();
}
