/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QPoint>
#include <QPolygonF>

#include "qt_conversions.h"

namespace hexnocular {
namespace algorithms {

//----------------------------------------------------------------------------------------------------------------------

static QPolygonF dot( const QPoint center, const unsigned rad )
{
    QPolygonF dot;
    return dot;
}

//----------------------------------------------------------------------------------------------------------------------

static std::vector<QPoint> grid( const QPoint dimension, const int radius, const int padding )
{
    int dots_x = ( 0.5 * radius + padding + 0.5 * radius );
    int dots_y = ( 0.5 * radius + padding + 0.5 * radius );
    // int dots_y = dimension.y() / ( padding + ( 2 * radius / 2 ));

    auto points = std::vector<QPoint>{};

    for( int x = 0; x <= dimension.x(); x += dots_x )
    {
        for( int y = 0; y <= dimension.y(); y += dots_y )
        {
            points.push_back( QPoint( x, y ) );
        }
    }
    return points;
}

//----------------------------------------------------------------------------------------------------------------------

static QPoint center( QPoint dimension )
{
    return QPoint( dimension.rx() / 2, dimension.ry() / 2 );
}

} // namespace algorithms
} // namespace hexnocular
