/**
 * This file is part of the hexnocular framework.
 *
 * Copyright (C) 2016 by Erik Kundt <bitshift@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <QObject>
#include <QQuickPaintedItem>
#include <QString>
#include <QPixmap>
#include <QPainter>

namespace hexnocular {

class WorkspaceImage : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(qreal tick READ tick WRITE setTick NOTIFY tickChanged)

public:
    WorkspaceImage( QQuickItem *parent = 0 );

    void    paint( QPainter *painter );

    void    setTick( const int& );
    int     tick() const { return _tick; }

signals:
    void    tickChanged();

private:
    int                     _tick;
    QPen                    _pen_white;
    QPen                    _pen_red;
    QPen                    _pen_green;
    QPen                    _current_pen;
    std::vector<QPolygonF>  _polygons;
};

} // namespace hexnocular
